
#include "MinesweeperBoard.h"
#ifndef PROJEKTY_MSBOARDTEXTVIEW_H
#define PROJEKTY_MSBOARDTEXTVIEW_H



class MSBoardTextView {

    MinesweeperBoard board;

public:
    MSBoardTextView(MinesweeperBoard board);

    MSBoardTextView();

    void display();

};


#endif //PROJEKTY_MSBOARDTEXTVIEW_H
