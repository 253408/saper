#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string>
#include "MSBoardTextView.h"
#include "MinesweeperBoard.h"
#include "MSTextController.h"

using namespace std;

MSTextController::MSTextController(MinesweeperBoard board, MSBoardTextView view)
        : board(board), view(view) {}

void MSTextController::play() {
    view.display();
    do {
        Command command = getUserInput();
        invokeCommand(command);
        view.display();
    } while (board.getGameState() == RUNNING);
    displayResult();
}

Command MSTextController::getUserInput() {
    Command command;
    command.x = readValueWithMax("x", board.getBoardWidth())-1;
    command.y = readValueWithMax("y", board.getBoardHeight())-1;
    command.actionType= readAction();
    return command;
}

int MSTextController::readValueWithMax(string dimension, int max) const {
    int tmp;
    do {
        cout << "Podaj wspolrzedna " << dimension << endl;
        cin >> tmp;
    } while (max<tmp);

    return tmp;
}

string MSTextController::readAction()  {
    string action;
    do{
        cout<<"Podaj akcje (f lub r)";
        cin>>action;
    }while(!(action == "f" || action =="r"));
    return action;
}

void MSTextController::invokeCommand(Command command) {
    if(command.actionType=="r") board.isRevealed(command.x,command.y);
    if(command.actionType=="f") board.toggleFlag(command.x,command.y);

}

void MSTextController::displayResult() {
    board.getGameState() == (FINISHED_WIN || FINISHED_LOSS);
}












