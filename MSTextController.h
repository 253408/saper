#include <string>
#include "MSBoardTextView.h"
#include "MinesweeperBoard.h"
#ifndef PROJEKTY_MSTEXTCONTROLLER_H
#define PROJEKTY_MSTEXTCONTROLLER_H


struct Command{
    int x;
    int y;
    std::string actionType;
};

class MSTextController {

    MinesweeperBoard board;
    MSBoardTextView view;
public:
    MSTextController(MinesweeperBoard board, MSBoardTextView view);
    void play();

    Command getUserInput();



    int readValueWithMax(std::string dimension,int max) const;

    std::string readAction();

    void invokeCommand(Command command);

    void displayResult();
};


#endif //PROJEKTY_MSTEXTCONTROLLER_H
