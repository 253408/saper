#include <iostream>
#include "MinesweeperBoard.h"
#include "MSBoardTextView.h"
#include "MSTextController.h"


using namespace std;

int main() {

    MinesweeperBoard board(10, 10, NORMAL);

    MSBoardTextView view(board);
    MSTextController ctrl(board, view);
    ctrl.play();

    return 0;
}
