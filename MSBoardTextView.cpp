
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string>
#include "MSBoardTextView.h"
#include "MinesweeperBoard.h"

using namespace std;

MSBoardTextView::MSBoardTextView(MinesweeperBoard board) : board(board) {}


void MSBoardTextView::display() {
    for (int w = 0; w < this->board.getBoardHeight(); w++) {
        for (int k = 0; k < this->board.getBoardWidth(); k++) {
            cout << board.getFieldInfo(w, k) << " ";
        }
        cout << endl;
    }
}




