
#include "MinesweeperBoard.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

using namespace std;

MinesweeperBoard::MinesweeperBoard(int width, int height, GameMode mode) {
    this->width = width;
    this->height = height;
    this->gameMode = mode;
    this->gameState = RUNNING;
    this->actionCount = 0;
    clearBoard();
    if (gameMode == DEBUG) {
        this->generateDebugBoard();
    } else {
        this->generateRandomBoard();
    }

}

void MinesweeperBoard::clearBoard() {
    for (int w = 0; w < width; w++) {
        for (int k = 0; k < height; k++) {
            board[w][k].hasMine = false;
            board[w][k].isRevealed = false;
            board[w][k].hasFlag = false;
        }
    }
}

void MinesweeperBoard::generateRandomBoard() {
    srand(time(NULL));
    float percentageOfMines = getMinePercentage();
    this->minesCount = width * height * percentageOfMines;
    for (int t = 0; t < minesCount; t++) {
        generateRandomMine();
    }
}

void MinesweeperBoard::generateRandomMine() {
    bool foundEmptyField = false;
    do {
        int x = getRandom(width);
        int y = getRandom(height);
        if (!isSet(x, y)) {
            setMine(x, y);
            foundEmptyField = true;
        }
    } while (!foundEmptyField);
}

float MinesweeperBoard::getMinePercentage() const {
    float percentageOfMines;
    if (gameMode == EASY) {
        percentageOfMines = 0.1;
    } else if (gameMode == NORMAL) {
        percentageOfMines = 0.2;
    } else if (gameMode == HARD) {
        percentageOfMines = 0.3;
    }
    return percentageOfMines;
}

void MinesweeperBoard::debug_display() const {
    for (int w = 0; w < this->height; w++) {
        for (int k = 0; k < this->width; k++) {
            if (board[w][k].hasMine)
                cout << "[M";
            else
                cout << "[.";
            if (board[w][k].isRevealed)
                cout << "o";
            else
                cout << ".";
            if (board[w][k].hasFlag)
                cout << "f]";
            else
                cout << ".]";
        }
        cout << endl;
    }
}

int MinesweeperBoard::getRandom(int upperBound) {
    return rand() % upperBound;
}

bool MinesweeperBoard::isSet(int x, int y) {
    return board[x][y].hasMine;
}

void MinesweeperBoard::setMine(int x, int y) {
    board[x][y].hasMine = true;
}

void MinesweeperBoard::generateDebugBoard() {
    for (int j = 0; j < this->width; j++) {
        setMine(0, j);
        this->minesCount ++;
    }
    for (int j = 0; j < this->width; j = j + 2) {
        setMine(j, 0);
        this->minesCount ++;
    }

    int min;
    if (width > height) { min = height; } else { min = width; }
    for (int j = 0; j < min; j++) {
        setMine(j, j);
        this->minesCount ++;
    }

}

int MinesweeperBoard::getBoardWidth() const {
    return this->width;
}

int MinesweeperBoard::getBoardHeight() const {
    return this->height;
}

int MinesweeperBoard::getMineCount() const {
    return this->minesCount;
}

int MinesweeperBoard::countMines(int x, int y) const {
    if (!isRevealed(x, y)) return -1;
    if (isOutside(x, y)) return -1;
    int count = 0;
    int coordinates[8][2] = {
            {x - 1, y - 1},
            {x - 1, y},
            {x - 1, y + 1},
            {x,     y - 1},
            {x,     y + 1},
            {x + 1, y - 1},
            {x + 1, y},
            {x + 1, y + 1}
    };
    for (int i = 0; i < 8; i++) {
        int _x = coordinates[i][0];
        int _y = coordinates[i][1];
        if (!isOutside(_x, _y) && hasMine(_x, _y)) {
            count++;
        }
    }
    return count;
}

bool MinesweeperBoard::hasMine(int _x, int _y) const { return board[_x][_y].hasMine; }

bool MinesweeperBoard::isOutside(int x, int y) const {
    if (x > this->width || y > this->height || x < 0 || y < 0)
        return true;
    else {
        return false;
    }
}

bool MinesweeperBoard::hasFlag(int x, int y) const {
    if (isOutside(x, y) || !board[x][y].hasFlag || isRevealed(x, y)) {
        return false;
    }
    return board[x][y].hasFlag;
}

void MinesweeperBoard::toggleFlag(int x, int y) {
    if (isOutside(x, y) || isRevealed(x, y) || isGameFinished()) {
        return;
    } else if (!isRevealed(x, y)) {
        board[x][y].isRevealed = true;
    }

}

bool MinesweeperBoard::isGameFinished() {
    return this->gameState == FINISHED_WIN || this->gameState == FINISHED_LOSS;
}

void MinesweeperBoard::revealField(int x, int y) {
    actionCount++;
    if (isRevealed(x, y) || isOutside(x, y) || isGameFinished() || board[x][y].hasFlag)
        return;
    if (!isRevealed(x, y) && !board[x][y].hasMine) {
        board[x][y].isRevealed = true;
        gameState = FINISHED_LOSS;
    }
    if (!isRevealed(x, y) && board[x][y].hasMine) {
        if (gameMode != DEBUG && actionCount == 1) {
            moveMineToAnotherField(x, y);
        } else {
            board[x][y].isRevealed = true;
            gameState = FINISHED_LOSS;
        }
    }
}

void MinesweeperBoard::moveMineToAnotherField(int x, int y) {
    generateRandomMine();
    board[x][y].hasMine = false;
}

bool MinesweeperBoard::isRevealed(int x, int y) const {
    return board[x][y].isRevealed;
}

GameState MinesweeperBoard::getGameState() const {
    if (gameState == FINISHED_LOSS)
        return FINISHED_LOSS;
    else if (evaluateGameState() == FINISHED_WIN)
        return FINISHED_WIN;
    else
        return RUNNING;
}

GameState MinesweeperBoard::evaluateGameState() const {
    if (actionCount + getMineCount() == width * height) {
        return FINISHED_WIN;
    } else {
        int correctFlagCount = 0;
        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                if (hasFlag(i, j) && hasMine(i, j)) {
                    correctFlagCount++;
                }

            }
        }
        if (correctFlagCount == minesCount) {
            return FINISHED_WIN;
        } else
            return RUNNING;
    }
}

char MinesweeperBoard::getFieldInfo(int x, int y) const {
    if (isOutside(x, y)) return '#';
    if (!isRevealed(x, y)) {
        if (hasFlag(x, y)) return 'F';
        else return '_';
    } else {
        if (hasMine(x, y)) return 'x';
        else if (countMines(x, y) == 0) return ' ';
        else return std::to_string(countMines(x, y)).at(0);
    }
}



